-- +goose Up

/* create user accounts table */
CREATE TABLE users (
    id int NOT NULL PRIMARY KEY,
    username text,
    name text,
    surname text
);

/*********************** 
  insert some users:
    -- Root User
    -- Jitek Vojtech
 ***********************/

INSERT INTO users VALUES
(0, 'root', '', ''),
(1, 'vojtechvitek', 'Vojtech', 'Vitek');

-- +goose Down
DROP TABLE 
/* testing embedded block comment */
users;
