package goose

import (
	"bufio"
	"bytes"
	"io"
	"regexp"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

type parserState int

const (
	start                   parserState = iota // 0
	gooseUp                                    // 1
	gooseStatementBeginUp                      // 2
	gooseStatementEndUp                        // 3
	gooseDown                                  // 4
	gooseStatementBeginDown                    // 5
	gooseStatementEndDown                      // 6
	openBlockComment                           // 7
	closeBlockComment                          // 8
)

var comment = false

type stateMachine []parserState

func (s *stateMachine) Pop() (top parserState) {
	top = (*s)[len(*s)-1]
	*s = (*s)[:len(*s)-1]
	return top
}
func (s *stateMachine) Push(new parserState) {
	verboseInfo("StateMachine: push(%v) => %v", new, s)
	*s = append(*s, new)
}

func (s stateMachine) Top() parserState {
	return s[len(s)-1]
}

func (s stateMachine) Prev(n int) parserState {
	if n > len(s) {
		return start
	}
	return s[len(s)-n-1]
}

func (s stateMachine) Contains(state parserState) bool {
	for _, x := range s {
		if x == state {
			return true
		}
	}
	return false
}

const scanBufSize = 4 * 1024 * 1024

var matchEmptyLines = regexp.MustCompile(`^\s*$`)

var bufferPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, scanBufSize)
	},
}

// Split given SQL script into individual statements and return
// SQL statements for given direction (up=true, down=false).
//
// The base case is to simply split on semicolons, as these
// naturally terminate a statement.
//
// However, more complex cases like pl/pgsql can have semicolons
// within a statement. For these cases, we provide the explicit annotations
// 'StatementBegin' and 'StatementEnd' to allow the script to
// tell us to ignore semicolons.
func parseSQLMigration(r io.Reader, direction bool) (stmts []string, useTx bool, err error) {
	var buf bytes.Buffer
	scanBuf := bufferPool.Get().([]byte)
	defer bufferPool.Put(scanBuf)

	scanner := bufio.NewScanner(r)
	scanner.Buffer(scanBuf, scanBufSize)

	stateMachine := &stateMachine{start}
	useTx = true

	for scanner.Scan() {
		line := scanner.Text()
		if verbose {
			log.Println(line)
		}

		if strings.Contains(line, "/*") {
			if stateMachine.Top() == openBlockComment {
				return nil, false, errors.Errorf("found repeated open block comment; stateMachine = %v", stateMachine)
			}
			if !strings.Contains(line, "*/") { // open multi-line block comment
				stateMachine.Push(openBlockComment)
			}
		} else if strings.Contains(line, "*/") { // close multi-line block comment
			if stateMachine.Top() != openBlockComment {
				return nil, false, errors.Errorf("found close block comment without matching open; stateMachine = %v", stateMachine)

			}
			stateMachine.Push(closeBlockComment)
		}

		// ignore line comments inside a block comment
		if strings.HasPrefix(line, "--") && stateMachine.Top() != openBlockComment {
			cmd := strings.TrimSpace(strings.TrimPrefix(line, "--"))

			if stateMachine.Top() == closeBlockComment {
				stateMachine.Pop() // pop close
				stateMachine.Pop() // pop open
			}

			switch cmd {
			case "+goose Up":
				comment = false
				if stateMachine.Contains(gooseUp) {
					return nil, false, errors.Errorf("duplicate '-- +goose Up' annotations; stateMachine=%v, see https://github.com/pressly/goose#sql-migrations", stateMachine)
				}
				stateMachine.Push(gooseUp)
				continue

			case "+goose Down":
				comment = false
				if !stateMachine.Contains(gooseUp) {
					return nil, false, errors.Errorf("must start with '-- +goose Up' annotation, stateMachine=%v, see https://github.com/pressly/goose#sql-migrations", stateMachine)
				}
				switch stateMachine.Top() {
				case gooseUp, gooseStatementEndUp:
					stateMachine.Push(gooseDown)
				default:
					return nil, false, errors.Errorf("must start with '-- +goose Up' annotation, stateMachine=%v, see https://github.com/pressly/goose#sql-migrations", stateMachine)
				}
				continue

			case "+goose StatementBegin":
				comment = false
				switch stateMachine.Top() {
				case gooseUp, gooseStatementEndUp:
					stateMachine.Push(gooseStatementBeginUp)
				case gooseDown, gooseStatementEndDown:
					stateMachine.Push(gooseStatementBeginDown)
				default:
					return nil, false, errors.Errorf("'-- +goose StatementBegin' must be defined after '-- +goose Up' or '-- +goose Down' annotation, stateMachine=%v, see https://github.com/pressly/goose#sql-migrations", stateMachine)
				}
				continue

			case "+goose StatementEnd":
				comment = false
				switch stateMachine.Top() {
				case gooseStatementBeginUp:
					stateMachine.Push(gooseStatementEndUp)
				case gooseStatementBeginDown:
					stateMachine.Push(gooseStatementEndDown)
				default:
					return nil, false, errors.New("'-- +goose StatementEnd' must be defined after '-- +goose StatementBegin', see https://github.com/pressly/goose#sql-migrations")
				}
			case "+goose NO TRANSACTION":
				comment = false
				useTx = false
				continue

			default:
				// Ignore leading comments (before '+goose Up') or apparent line comments inside a block comment
				if stateMachine.Top() == start || stateMachine.Top() == openBlockComment {
					continue
				}
				// Include line comments within stored procedure and function object bodies
				verboseInfo("StateMachine: found line comment")
				comment = true
			}
		}

		// Ignore empty lines.
		if matchEmptyLines.MatchString(line) {
			verboseInfo("StateMachine: ignore empty line")
			continue
		}

		// Write SQL line to a buffer, excluding Goose directives.
		if !strings.Contains(line, "+goose") {
			verboseInfo("line: %s", line)
			if _, err := buf.WriteString(line + "\n"); err != nil {
				return nil, false, errors.Wrap(err, "failed to write to buf")
			}
		}

		// Read SQL body line by line, if we're in the right direction.
		//
		// 1) basic query with semicolon; 2) psql/tsql statement
		//
		// Export statement once we hit end of statement.
		switch stateMachine.Top() {
		case gooseUp, gooseStatementBeginUp, gooseStatementEndUp:
			if !direction /*down*/ {
				buf.Reset()
				verboseInfo("StateMachine: ignore gooseUp, gooseStatementBeginUp, gooseStatementEndUp on down")
				continue
			}
		case gooseDown, gooseStatementBeginDown, gooseStatementEndDown:
			if direction /*up*/ {
				buf.Reset()
				verboseInfo("StateMachine: ignore gooseDown, gooseStatementBeginDown, gooseStatementEndDown on up")
				continue
			}
		case openBlockComment:
			if stateMachine.Prev(1) == start {
				buf.Reset()
				verboseInfo("StateMachine: ignore open block comment in preamble")
				continue
			}
		case closeBlockComment:
			if stateMachine.Prev(1) == openBlockComment && stateMachine.Prev(2) == start {
				buf.Reset()
				verboseInfo("StateMachine: ignore close block comment in preamble")
				stateMachine.Pop() // pop closeBlockComment
				stateMachine.Pop() // pop openBlockComment
				continue
			}
		default:
			if !comment && stateMachine.Top() != start {
				return nil, false, errors.Errorf("failed to parse migration: unexpected state %v on line %q, see https://github.com/pressly/goose#sql-migrations", stateMachine, line)
			}
		}

		switch stateMachine.Top() {
		case gooseUp:
			if endsWithSemicolon(line) {
				stmts = append(stmts, buf.String())
				buf.Reset()
				verboseInfo("StateMachine: store simple Up query")
			}
		case gooseDown:
			if endsWithSemicolon(line) {
				stmts = append(stmts, buf.String())
				buf.Reset()
				verboseInfo("StateMachine: store simple Down query")
			}
		case gooseStatementEndUp:
			stmts = append(stmts, buf.String())
			buf.Reset()
			verboseInfo("StateMachine: store Up statement")
			stateMachine.Pop() // pop end statement
			stateMachine.Pop() // pop begin statement
		case gooseStatementEndDown:
			stmts = append(stmts, buf.String())
			buf.Reset()
			verboseInfo("StateMachine: store Down statement")
			stateMachine.Pop() // pop end statement
			stateMachine.Pop() // pop begin statement
		case openBlockComment:
			verboseInfo("StateMachine: found open block comment")
		case closeBlockComment:
			verboseInfo("StateMachine: found close block comment")
			stateMachine.Pop() // pop close block
			stateMachine.Pop() // pop open block

		default:
			if (comment || stateMachine.Top() == closeBlockComment) && stateMachine.Top() != gooseStatementBeginUp && stateMachine.Top() != gooseStatementBeginDown {
				stmts = append(stmts, buf.String())
				buf.Reset()
				verboseInfo("StateMachine: store line comment")
				comment = false
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, false, errors.Wrap(err, "failed to scan migration")
	}
	// EOF

	switch stateMachine.Top() {
	//	case start:
	//		return nil, false, errors.New("failed to parse migration: must start with '-- +goose Up' annotation, see https://github.com/pressly/goose#sql-migrations")
	case gooseStatementBeginUp:
		return nil, false, errors.New("failed to parse migration: missing '-- +goose StatementEnd' annotation in +goose Up")
	case gooseStatementBeginDown:
		return nil, false, errors.New("failed to parse migration: missing '-- +goose StatementEnd' annotation in +goose Down")
	case openBlockComment:
		return nil, false, errors.New("failed to parse migration: unclosed block comment")
	}

	if bufferRemaining := strings.TrimSpace(buf.String()); len(bufferRemaining) > 0 {
		if bufferRemaining := strings.TrimSpace(buf.String()); len(bufferRemaining) > 0 {
			return nil, false, errors.Errorf("failed to parse migration: state %v, direction: %v: unexpected unfinished SQL query: %q: missing semicolon?", stateMachine, direction, bufferRemaining)
		}
	}

	if len(stmts) > 0 && strings.Contains(stmts[len(stmts)-1], "+goose StatementEnd") {
		verboseInfo("ignore ", stmts[len(stmts)-1])
	}

	verboseInfo("stmts %v", stmts)
	return stmts, useTx, nil
}

// Checks the line to see if the line has a statement-ending semicolon
// or if the line contains a double-dash comment.
func endsWithSemicolon(line string) bool {
	scanBuf := bufferPool.Get().([]byte)
	defer bufferPool.Put(scanBuf)

	prev := ""
	scanner := bufio.NewScanner(strings.NewReader(line))
	scanner.Buffer(scanBuf, scanBufSize)
	scanner.Split(bufio.ScanWords)

	// allow for trailing comments after a statement
	for scanner.Scan() {
		word := scanner.Text()
		if strings.HasPrefix(word, "--") {
			break
		}
		prev = word
	}

	return strings.HasSuffix(prev, ";")
}
